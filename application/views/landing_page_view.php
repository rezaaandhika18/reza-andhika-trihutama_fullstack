<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>FlexStart Bootstrap Template - Index</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets_landing/img/favicon.png" rel="icon">
  <link href="assets_landing/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="assets_landing/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets_landing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets_landing/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets_landing/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets_landing/css/style.css" rel="stylesheet">
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
      <a href="index.html" class="logo d-flex align-items-center">
        <img src="assets_landing/img/logo.png" alt="">
      </a>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="<?=base_url()?>">Home</a></li>
          <li><a class="nav-link scrollto" href="<?=base_url('article')?>">Article</a></li>
          <li><a class="getstarted scrollto" href="<?=base_url('sign_in')?>">Sign In</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center">
          <h1 data-aos="fade-up">Nova Content Management System</h1>
          <h2 data-aos="fade-up" data-aos-delay="400">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </h2>
          <div data-aos="fade-up" data-aos-delay="600">
            <div class="text-center text-lg-start">
            </div>
          </div>
        </div>
        <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="assets_landing/img/hero-img.png" class="img-fluid" alt="">
        </div>
        <!-- How Is Work -->
        <div class="row feture-tabs" data-aos="fade-up">
          <div class="col-lg-12">
            <h3>How Its Work</h3>
          </div>
          <div class="col-lg-4">
            <h4>1. Create Account</h4>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</p>
          </div>
          <div class="col-lg-4">
            <h4>2. Make a Content</h4>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</p>
          </div>
          <div class="col-lg-4">
            <h4>3. Publish It</h4>
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</p>
          </div>
        </div><!-- End How Is Work Tabs -->
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Values Section ======= -->
    <section id="values" class="values">
      <div class="container" data-aos="fade-up">
        <header class="section-header">
          <h2>Value</h2>
          <p>Odit est perspiciatis laborum et dicta</p>
        </header>
        <div class="row">
          <div class="col-lg-3" data-aos="fade-up" data-aos-delay="200">
            <div class="box">
              <img src="assets_landing/img/values-1.png" class="img-fluid" alt="">
              <h3>Creative Design</h3>
              <p>Eum ad dolor et. Autem aut fugiat debitis voluptatem consequuntur sit. Et veritatis id.</p>
            </div>
          </div>

          <div class="col-lg-3 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="400">
            <div class="box">
              <img src="assets_landing/img/values-2.png" class="img-fluid" alt="">
              <h3>Just Write It</h3>
              <p>Repudiandae amet nihil natus in distinctio suscipit id. Doloremque ducimus ea sit non.</p>
            </div>
          </div>

          <div class="col-lg-3 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="600">
            <div class="box">
              <img src="assets_landing/img/values-3.png" class="img-fluid" alt="">
              <h3>Responsive Layout</h3>
              <p>Quam rem vitae est autem molestias explicabo debitis sint. Vero aliquid quidem commodi.</p>
            </div>
          </div>

          <div class="col-lg-3 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="800">
            <div class="box">
              <img src="assets_landing/img/values-4.png" class="img-fluid" alt="">
              <h3>Award System</h3>
              <p>Quam rem vitae est autem molestias explicabo debitis sint. Vero aliquid quidem commodi.</p>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Values Section -->

    <!-- ======= Features Section ======= -->
    <section id="features" class="features">
      <div class="container" data-aos="fade-up">
        <header class="section-header">
          <h2>Features</h2>
          <p>Laboriosam et omnis fuga quis dolor direda fara</p>
        </header>
        <div class="row">
          <div class="row feture-tabs" data-aos="fade-up">
            <div class="col-lg-6">
              <img src="assets_landing/img/features.png" class="img-fluid p-4" alt="">
            </div>
            <div class="col-lg-6">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="row feture-tabs" data-aos="fade-up">
            <div class="col-lg-6">
              <h3>Manage All Content With Creativity</h3>
              <br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
                Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
            <div class="col-lg-6">
              <img src="assets_landing/img/features-2.png" class="img-fluid p-4" alt="">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="row feture-tabs" data-aos="fade-up">
            <div class="col-lg-6">
              <img src="assets_landing/img/features-3.png" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6">
              <h3>The Best Content Management System</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Features Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="fade-up">
        <header class="section-header">
          <h2>Testimonials</h2>
          <p>What they are saying about us</p>
        </header>
        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="200">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="testimonial-item">
                <div class="stars">
                  <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                </div>
                <p>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                </p>
                <div class="profile mt-auto">
                  <img src="assets_landing/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
                  <h3>Saul Goodman</h3>
                  <h4>Ceo &amp; Founder</h4>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="testimonial-item">
                <div class="stars">
                  <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                </div>
                <p>
                  Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                </p>
                <div class="profile mt-auto">
                  <img src="assets_landing/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
                  <h3>Sara Wilsson</h3>
                  <h4>Designer</h4>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="testimonial-item">
                <div class="stars">
                  <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                </div>
                <p>
                  Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                </p>
                <div class="profile mt-auto">
                  <img src="assets_landing/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
                  <h3>Jena Karlis</h3>
                  <h4>Store Owner</h4>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="testimonial-item">
                <div class="stars">
                  <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                </div>
                <p>
                  Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                </p>
                <div class="profile mt-auto">
                  <img src="assets_landing/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
                  <h3>Matt Brandon</h3>
                  <h4>Freelancer</h4>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="testimonial-item">
                <div class="stars">
                  <i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i>
                </div>
                <p>
                  Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
                </p>
                <div class="profile mt-auto">
                  <img src="assets_landing/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
                  <h3>John Larson</h3>
                  <h4>Entrepreneur</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>
    </section>
  </main>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="container">
      <div class="copyright">
        <div class="social-links mt-3">
          <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
          <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
          <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
          <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
        </div>
        &copy; Copyright 2022 <strong><span>Nova</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets_landing/vendor/aos/aos.js"></script>
  <script src="assets_landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets_landing/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets_landing/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets_landing/js/main.js"></script>
</body>
</html>