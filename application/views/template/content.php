        <section role="main" class="content-body">
          <header class="page-header">
            <h2><?php echo $title?></h2>
          
            <div class="right-wrapper pull-right">
              <ol class="breadcrumbs">
                <li>
                  <a href="<?php echo base_url('home')?>">
                    <i class="fa fa-home"></i>
                  </a>
                </li>
                <li><span><?php echo $title?></span></li>
              </ol>
          
              <a class="sidebar-right-toggle"></a>
            </div>
          </header>
          <!-- start: page -->
          <?php echo $contents;?>
          <!-- end: page -->
        </section>