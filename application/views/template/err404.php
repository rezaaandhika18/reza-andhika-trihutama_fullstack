<section class="body-error error-inside">
  <div class="center-error">

    <div class="row">
      <div class="col-md-12">
        <div class="main-error mb-xlg">
          <p class="error-explanation text-center">Oops! Page Not Found</p>
          <h2 class="error-code text-dark text-center text-semibold m-none">404 <i class="fa fa-file"></i></h2>
          <p class="error-explanation text-center">We're sorry, but the page you were looking for doesn't exist</p>
        </div>
      </div>
    </div>
  </div>
</section>