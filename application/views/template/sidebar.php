      <!-- start: sidebar -->
      <aside id="sidebar-left" class="sidebar-left">
        <div class="sidebar-header">
          <div class="sidebar-title">
            Navigation
          </div>
          <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
          </div>
        </div>

        <div class="nano">
          <div class="nano-content">
            <nav id="menu" class="nav-main" role="navigation">
              <ul class="nav nav-main">
                <li>
                  <a href="<?php echo base_url('home')?>">
                    <i class="fas fa-home" aria-hidden="true"></i>
                    <span>Home</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('article/create')?>">
                    <i class="fas fa-plus" aria-hidden="true"></i>
                    <span>Create Article</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo base_url('article/list')?>">
                    <i class="far fa-folder-open" aria-hidden="true"></i>
                    <span>List Article</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </aside>
      <!-- end: sidebar -->