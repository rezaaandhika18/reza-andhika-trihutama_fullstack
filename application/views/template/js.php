    <script src="<?=base_url('assets/vendor/jquery/jquery.js')?>"></script>
    <script src="<?=base_url('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')?>"></script>
    <script src="<?=base_url('assets/vendor/bootstrap/js/bootstrap.js')?>"></script>
    <script src="<?=base_url('assets/vendor/nanoscroller/nanoscroller.js')?>"></script>
    <?php if($this->uri->segment(1) == 'article' && $this->uri->segment(2) == 'create'){ ?>
        <script src="<?=base_url('assets/vendor/ckeditor/ckeditor.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/plugins/sortable.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/fileinput.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/locales/fr.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/locales/es.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/themes/fas/theme.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/themes/explorer-fas/theme.js')?>"></script>
        <script type="text/javascript">
            var configForContent = {
                height: 300,
                font_defaultLabel: "Arial",
                font_names: "Arial;",
                fontSize_defaultLabel: "14px",
                fontSize_sizes: "14/14px;",
                toolbarGroups: [{
                    name: "document",
                    groups: ["mode", "document", "doctools"]
                }, {
                    name: "editing",
                    groups: ["find", "selection", "spellchecker", "editing"]
                }, {
                    name: "basicstyles",
                    groups: ["basicstyles", "cleanup"]
                }, {
                    name: "clipboard",
                    groups: ["clipboard", "undo"]
                }, {
                    name: "forms",
                    groups: ["forms"]
                }, {
                    name: "paragraph",
                    groups: ["list", "indent", "blocks", "align", "bidi", "paragraph"]
                }, {
                    name: "links",
                    groups: ["links"]
                }, {
                    name: "insert",
                    groups: ["insert"]
                }, {
                    name: "styles",
                    groups: ["styles"]
                }, {
                    name: "colors",
                    groups: ["colors"]
                }, {
                    name: "tools",
                    groups: ["tools"]
                }, {
                    name: "others",
                    groups: ["others"]
                }, {
                    name: "about",
                    groups: ["about"]
                }],
                removeButtons: "Unlink,Language,Cut,Templates,NewPage,Print,Copy,Paste,PasteText,PasteFromWord,Scayt,Indent,Outdent,CopyFormatting,RemoveFormat,Flash,Smiley,PageBreak,Iframe,Styles,Format,ShowBlocks,About,BGColor,TextColor,HiddenField,Button,Form,Checkbox,Radio,TextField,Textarea,Select,ImageButton,CreateDiv,Subscript,Superscript,Save,Replace,Find,Undo,Redo,BidiRtl,BidiLtr"
            };
            $(document).ready(function (){
                CKEDITOR.replace("textcontent", configForContent)
            });

            $("#image").fileinput({
                theme: 'fas',
                allowedFileExtensions: ['png', 'jpg', 'jpeg'],
                dropZoneEnabled: false,
                minImageWidth: 540,
                autoReplace: true,
                overwriteInitial: true,
                showUploadedThumbs: false,
                maxFileSize: 5000,
                maxFileCount: 1,
                browseClass: "btn btn-sm btn-primary",
                showCaption: false,
                required: true,
                showRemove: false,
                showUpload: false,
                slugCallback: function (filename) {
                    return filename.replace('(', '_').replace(']', '_');
                }
            });
        </script>
    <?php } ?>
    <?php if($this->uri->segment(1) == 'article' && $this->uri->segment(2) == 'list'){ ?>
        <script src="<?=base_url('assets/vendor/datatables.net/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?=base_url('assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
        <script src="<?=base_url('assets/vendor/datatables.net-responsive/js/dataTables.responsive.min.js')?>"></script>
        <script src="<?=base_url('assets/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js')?>"></script>
        <script src="<?=base_url('assets/vendor/datatables.net-scroller/js/dataTables.scroller.min.js')?>"></script>
        <script type="text/javascript">
            setTimeout(function(){
                var tabel = $('#datatable-user-responsive').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "responsive": true,
                    "oLanguage": {
                        sLengthMenu: '_MENU_ records per page',
                        sProcessing: '<i class="fa fa-spinner fa-spin"></i> Loading'
                    },
                    "language": {
                        paginate: {
                          next: "<i class='fa fa-chevron-right'></i>", 
                          previous: "<i class='fa fa-chevron-left'></i>" 
                        }
                    },
                    "searchDelay": 1800, 
                    "ordering": true, 
                    "order": [[ 0, 'desc' ]], 
                    "ajax":{
                        "url": "<?=base_url('article/get/list')?>",
                        "type": "POST",
                        "data": function(e){
                            e.<?=$this->security->get_csrf_token_name();?> = $("input[name=<?=$this->security->get_csrf_token_name();?>]").val();
                        },
                        "dataSrc": function (r) {
                            //refresh csrf
                            updtCSRF();
                            
                            return r.data;
                        },
                        "error": function (xhr, error, thrown) {
                            if(thrown == 'Forbidden'){
                                alert('The action you have requested is not allowed. Please refresh your page');
                            }
                            else{
                                alert(xhr.responseText);
                                updtCSRF();
                            }
                        }
                    },
                    "deferRender": true,
                    "aLengthMenu": [[5, 10, 50],[ 5, 10, 50]],
                    "columns": [
                        { render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { "data": "title" },
                        { "data": "slug" },
                        { render: function ( data, type, row, meta ) 
                            {
                                imageName = row.image_name;
                                imageHash = row.image_hash;
                                imagehref = '<a target="_blank" href="<?=base_url('/upload/')?>'+imageHash+'">'+imageName+'</a>';
                                return imagehref;
                            }
                        },
                        { "data": "created_by" },
                        { "data": "created_date" },
                        { "data": "last_update" },
                        { render: function ( data, type, row, meta ) 
                            {
                                return row.is_active == 1 ? 'Active' : 'Non-active';
                            }
                        },
                        { render: function ( data, type, row, meta ) 
                            {
                                var html = '';
                                html += '<a href="<?=base_url()?>article/edit?id='+btoa(btoa(row.id))+'" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" title="edit">'
                                +'<i class="fas fa-edit"></i> Edit</a>'+
                                '<a href="<?=base_url()?>article/delete?id='+btoa(btoa(row.id))+'" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" title="delete" onclick="isconfirm()">'
                                +'<i class="fas fa-minus-circle"></i> Delete</a>';
                                return html;
                            }
                        }
                    ],
                });
            }, 1500);

            var isconfirm = () => {
                confirm('Are you sure you want to delete this user?');
            }
        </script>
    <?php } ?>
    <?php if($this->uri->segment(1) == 'article' && $this->uri->segment(2) == 'edit'){ ?>
        <script src="<?=base_url('assets/vendor/ckeditor/ckeditor.js')?>" type="text/javascript"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/plugins/sortable.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/fileinput.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/locales/fr.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/js/locales/es.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/themes/fas/theme.js')?>"></script>
        <script src="<?=base_url('assets/vendor/bootstrap-fileinput/themes/explorer-fas/theme.js')?>"></script>
        <script type="text/javascript">
            var configForContent = {
                height: 300,
                font_defaultLabel: "Arial",
                font_names: "Arial;",
                fontSize_defaultLabel: "14px",
                fontSize_sizes: "14/14px;",
                toolbarGroups: [{
                    name: "document",
                    groups: ["mode", "document", "doctools"]
                }, {
                    name: "editing",
                    groups: ["find", "selection", "spellchecker", "editing"]
                }, {
                    name: "basicstyles",
                    groups: ["basicstyles", "cleanup"]
                }, {
                    name: "clipboard",
                    groups: ["clipboard", "undo"]
                }, {
                    name: "forms",
                    groups: ["forms"]
                }, {
                    name: "paragraph",
                    groups: ["list", "indent", "blocks", "align", "bidi", "paragraph"]
                }, {
                    name: "links",
                    groups: ["links"]
                }, {
                    name: "insert",
                    groups: ["insert"]
                }, {
                    name: "styles",
                    groups: ["styles"]
                }, {
                    name: "colors",
                    groups: ["colors"]
                }, {
                    name: "tools",
                    groups: ["tools"]
                }, {
                    name: "others",
                    groups: ["others"]
                }, {
                    name: "about",
                    groups: ["about"]
                }],
                removeButtons: "Unlink,Language,Cut,Templates,NewPage,Print,Copy,Paste,PasteText,PasteFromWord,Scayt,Indent,Outdent,CopyFormatting,RemoveFormat,Flash,Smiley,PageBreak,Iframe,Styles,Format,ShowBlocks,About,BGColor,TextColor,HiddenField,Button,Form,Checkbox,Radio,TextField,Textarea,Select,ImageButton,CreateDiv,Subscript,Superscript,Save,Replace,Find,Undo,Redo,BidiRtl,BidiLtr"
            };
            
            $(document).ready(function (){
                CKEDITOR.replace("textcontent", configForContent)
            });

            $("#image").fileinput({
                theme: 'fas',
                allowedFileExtensions: ['png', 'jpg', 'jpeg'],
                dropZoneEnabled: false,
                minImageWidth: 540,
                autoReplace: true,
                overwriteInitial: true,
                showUploadedThumbs: false,
                maxFileSize: 5000,
                maxFileCount: 1,
                browseClass: "btn btn-sm btn-primary",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                slugCallback: function (filename) {
                    return filename.replace('(', '_').replace(']', '_');
                },
                initialPreviewAsData: true,
                initialPreview: [
                    '<?=base_url('/upload/'.$articleObj->image_hash)?>',
                ],
                initialPreviewConfig: [
                    {caption: "<?=$articleObj->image_name?>", showRemove: false}
                ]
            });
        </script>
    <?php } ?>
    <script src="<?=base_url('assets/javascripts/theme.js')?>"></script>
    <script src="<?=base_url('assets/javascripts/theme.init.js')?>"></script>
    <script type="text/javascript">
        setTimeout(function(){
            $(document).ready(function() {
                $('#loading').hide();
            });
            $("a").bind("contextmenu", function(e){
            e.preventDefault();
                return false;
            });
            $('a').click(function(event){
                event.preventDefault();
                window.location = $(this).attr('href');
            });
        }, 800);

        let updtCSRF = () => {
            const service_url = '<?=base_url('/updateCSRF')?>';
            $.ajax({
                type : "GET",
                url : service_url,
                dataType: "json",
                async: false,
                success: function(res){
                    $("input[name=<?=$this->security->get_csrf_token_name();?>]").val(res.<?=$this->security->get_csrf_token_name();?>);
                },
                error: function(e){
                    alert(e.responseText);
                    alert('Please contact administrator to fix issue');
                },
            });
        }
    </script>