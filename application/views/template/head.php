<head>
    <meta charset="UTF-8">
    <title>Application CMS Article</title>
    <meta name="keywords" content="HTML5 CMS Template" />
    <meta name="description" content="CMS Application System">
    <meta name="author" content="CMS">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link href="<?=base_url('assets/stylesheets/font-gadis.css')?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" href="<?=base_url('assets/vendor/font-awesome/css/all.min.css')?>" />
    <!-- Favicons -->
    <link href="assets_landing/img/favicon.png" rel="icon">
    <link href="assets_landing/img/apple-touch-icon.png" rel="apple-touch-icon">
    <?php if($this->uri->segment(1) == 'article' && $this->uri->segment(2) == 'create'){ ?>
        <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap-fileinput/css/fileinput.min.css')?>" />
        <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap-fileinput/themes/explorer-fas/theme.css')?>" media="all" />
    <?php } ?>
    <?php if($this->uri->segment(1) == 'article' && $this->uri->segment(2) == 'list'){ ?>
        <link href="<?=base_url('assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?=base_url('assets/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?=base_url('assets/vendor/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')?>" rel="stylesheet">
    <?php } ?>
    <?php if($this->uri->segment(1) == 'article' && $this->uri->segment(2) == 'edit'){ ?>
        <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap-fileinput/css/fileinput.min.css')?>" />
        <link rel="stylesheet" href="<?=base_url('assets/vendor/bootstrap-fileinput/themes/explorer-fas/theme.css')?>" media="all" />
    <?php } ?>
    <link rel="stylesheet" href="<?=base_url('assets/stylesheets/theme.css')?>" />
    <link rel="stylesheet" href="<?=base_url('assets/stylesheets/skins/default.css')?>" />
    <script src="<?=base_url('assets/vendor/modernizr/modernizr.js')?>"></script>
</head>