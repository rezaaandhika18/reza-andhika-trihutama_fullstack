<!doctype html>
<html class="fixed">
	<?php require 'template/head.php'; ?>
    <?php require 'template/js.php'; ?>
	<body>
		<section class="body-sign">
			<div class="center-sign">
				<a href="<?=base_url('login')?>" class="logo pull-left">
					<br/><img src="<?=base_url('assets/images/logo.png')?>" alt="Telkomsel" height="42">
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>
					<div class="panel-body">
						<div class="alert alert-info">
							<p class="m-none text-semibold h6">Fill this form to continue !</p>
						</div>

						<?=form_open('services/sign_up',array('method' => 'post','autocomplete'=>'off')); ?>
							<div class="form-group mb-lg">
								<label>Username <span class="required">*</span> <p class="m-none text-semibold h6">min : 4 character</p></label> 
								<div class="input-group input-group-icon">
									<input name="username" type="text" class="form-control input-lg" placeholder="ex : jakarta" required/>
									
								</div>
							</div>
							<div class="form-group mb-lg">
								<label>phone <span class="required">*</span></label>
								<div class="input-group input-group-icon">
									<input name="phone" type="text" class="form-control input-lg" placeholder="ex : 6281101xxxxx" required/>
								</div>
							</div>
							<div class="form-group mb-lg">
								<label>Passsword <span class="required">*</span><p class="m-none text-semibold h6">min : 8 character, combination uppercase, lowecase, symbol, numeric</p></label>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control input-lg" id="txtNewPassword" placeholder="ex : Jak2022!" required/>
								</div>
							</div>
							<div class="form-group mb-lg">
								<label>Retype - Passsword <span class="required">*</span></label>
								<div class="input-group input-group-icon">
									<input name="rytpepassword" type="password" class="form-control input-lg" id="txtConfirmPassword" placeholder="ex : Jak2022" required/>
								</div>
							</div>
							<div id="CheckPasswordMatch" style="display:none"></div>

							<div class="row">
								<div class="col-sm-4">
								</div>
								<div class="col-sm-4">
									<input type="submit" class="btn btn-primary hidden-xs" value="Submit">
									<input type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg" value="Reset Password">
								</div>
								<div class="col-sm-4 text-right">
								</div>
							</div>

							<p class="text-center mt-lg">Remembered? <a href="<?=base_url('login')?>">Sign In!</a></p>
						</form>
					</div>
				</div>
			</div>
		</section>
	</body>
	<script type="text/javascript">
		var checkPasswordMatch = () => {
			var password = $("#txtNewPassword").val()
			var confirmPassword = $("#txtConfirmPassword").val()
			$("#CheckPasswordMatch").show()
			if (password != confirmPassword)
				$("#CheckPasswordMatch").html('<p class="alert alert-danger">Passwords does not match!</p>')
			else
				$("#CheckPasswordMatch").html('<p class="alert alert-success">Passwords match</p>')
		}
		$(document).ready(function () {
			$("#txtConfirmPassword").keyup(checkPasswordMatch)
		});
	</script>
</html>
