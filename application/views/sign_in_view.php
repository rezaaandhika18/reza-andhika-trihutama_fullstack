<!doctype html>
<html class="fixed">
	<!-- head -->
	<?php require 'template/head.php'; ?>
	<!-- js -->
    <?php require 'template/js.php'; ?>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<img src="assets/images/logo.png" height="54" alt="Porto Admin" />
				</a>
				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
					</div>
					<div class="panel-body">
						<?php if(base64_decode($this->input->get('notifbox')) == 'false') { ?>
			            <div class="alert alert-danger">
			              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			              <p class="m-none text-semibold h6"><?= preg_replace("/\r\n|\r|\n/","<br>",base64_decode($this->input->get('reason')))?></p>
			            </div>
			            <?php } ?>
			            
						<?=form_open('services/sign_in',array('method' => 'post','accept-charset' => 'utf-8', 'role' => 'form')); ?>
							<div class="form-group mb-lg">
								<label>Username</label>
								<div class="input-group input-group-icon">
									<input name="username" type="text" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Password</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="password" type="password" class="form-control input-lg" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<a href="<?php echo base_url('sign_up');?>">Hello, Sign Up Here</a>
								</div>
								<div class="col-sm-2">
								</div>
								<div class="col-sm-4 text-right">
									<input type="submit" class="btn btn-primary hidden-xs" value="Login">
									<input type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg" value="Login">
								</div>
							</div>
						<?= form_close(); ?>
						<!-- </form> -->
					</div>
				</div>
			</div>
		</section>
		<!-- end: page -->
	</body>
</html>