<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>FlexStart Bootstrap Template - Index</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?=base_url()?>assets_landing/img/favicon.png" rel="icon">
  <link href="<?=base_url()?>assets_landing/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="<?=base_url()?>assets_landing/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_landing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_landing/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=base_url()?>assets_landing/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?=base_url()?>assets_landing/css/style.css" rel="stylesheet">
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
      <a href="index.html" class="logo d-flex align-items-center">
        <img src="<?=base_url()?>assets_landing/img/logo.png" alt="">
      </a>
      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="<?=base_url()?>">Home</a></li>
          <li><a class="nav-link scrollto" href="<?=base_url('article')?>">Article</a></li>
          <li><a class="getstarted scrollto" href="<?=base_url('sign_in')?>">Sign In</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->

  <main id="main">
    <section class="values">
      <div class="container" data-aos="fade-up">
        <header class="section-header">
        </header>
        <div class="box">
          <div class="row" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-6">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
            <div class="col-lg-6">
              <img src="<?=base_url()?>assets_landing/img/features.png" class="img-fluid p-4" alt="">
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container" data-aos="fade-up">
        <header class="section-header">
        </header>
        <div class="row" data-aos="fade-up" data-aos-delay="200">
          <div class="row col-lg-6 mt-6 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
          <div class="row col-lg-6 mt-6 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
          <div class="row col-lg-6 mt-6 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
          <div class="row col-lg-6 mt-6 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
          <div class="row col-lg-6 mt-6 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
          <div class="row col-lg-6 mt-6 mt-lg-0" data-aos="fade-up" data-aos-delay="200">
            <div class="col-lg-12">
              <h3>Easy To Use On All Your device</h3><br>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis earum delectus distinctio quidem possimus! Quidem, nulla alias! Itaque laudantium corporis dolorum.<br><br>
              Aut voluptates voluptatum amet rerum, vero consequatur, accusamus perspiciatis voluptatibus eum.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="container">
      <div class="copyright">
        <div class="social-links mt-3">
          <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
          <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
          <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
          <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
        </div>
        &copy; Copyright 2022 <strong><span>Nova</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?=base_url()?>assets_landing/vendor/aos/aos.js"></script>
  <script src="<?=base_url()?>assets_landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets_landing/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="<?=base_url()?>assets_landing/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?=base_url()?>assets_landing/js/main.js"></script>
</body>
</html>