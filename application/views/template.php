<?php
$is_logged_in = $this->session->userdata('is_active');
if(!isset($is_logged_in) || ($is_logged_in != true)){
	$stringUri = $this->uri->uri_string();
	if($stringUri!=""){
		echo '<script>alert("SESSION ANDA BELUM TERDAFTAR ATAU SUDAH EXPIRED , SILAHKAN LOGIN TERLEBIH DAHULU");</script>';
		//Kick Access to Login Page
		$this->session->sess_destroy();
		redirect('/login', 'refresh');
	}
}
?>

<!doctype html>
<html class="fixed">
	<!-- head -->
	<?php require 'template/head.php'; ?>
	<div id="loading" >
  		<img id="loading-image" src="<?=base_url('assets/images/loader.gif')?>" alt="Loading..." />
	</div>
	<body>
		<section class="body">
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="<?=base_url('home')?>" class="logo">
						<img src="<?=base_url('assets/images/logo.png')?>" height="35" alt="CMS" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
				<!-- start: search & user box -->
				<div class="header-right">
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="<?=base_url('assets/images/profile_user.png')?>" alt="APH/GADIS" class="img-circle" data-lock-picture="<?=base_url('assets/images/profile_user.png')?>" />
							</figure>
							<div class="profile-info">
								<span class="name"><?=ucwords($this->session->userdata('username'))?></span>
							</div>
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="<?=base_url('sign_out')?>"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->
			<div class="inner-wrapper">
				<!-- sidebar -->
    			<?php require 'template/sidebar.php'; ?>
				<!-- content -->
				<?php require 'template/content.php'; ?>
			</div>
		</section>
	<!-- js  -->
    <?php require 'template/js.php'; ?>
	</body>
</html>