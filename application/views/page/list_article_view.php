<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
        </div>
        <h2 class="panel-title"><?php echo ucwords(strtolower($title))?></h2>
      </header>
      <div class="panel-body">
        <?php if($this->input->get('success_box') == md5('true')){?>
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <strong>Well done!</strong> <?=base64_decode($this->input->get('reason'))?>
        </div>
        <?php } elseif($this->input->get('success_box') == md5('false')) { ?>
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?php echo preg_replace("/\r\n|\r|\n/","<br>",base64_decode($this->input->get('reason')))?>
        </div>
        <?php } ?>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <table id="datatable-user-responsive" class="table table-bordered table-striped dt-responsive" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>No</th>
              <th>Title</th>
              <th>Slug</th>
              <th>Image</th>
              <th>Created By</th>
              <th>Created Date</th>
              <th>Last Update</th>
              <th>Active</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </section>
  </div>
</div>