<div class="row">
  <div class="col-md-4 col-lg-3">
    <section class="panel">
      <div class="panel-body">
        <div class="thumb-info mb-md">
          <img src="<?=base_url("/assets/images/profile_user.png")?>" class="rounded img-responsive" alt="Application CMS">
          <div class="thumb-info-title">
            <span class="thumb-info-inner"><?php echo $this->session->userdata('username')?></span>
          </div>
        </div>
        <hr class="dotted short">
        <h6 class="text-muted"><strong>Your Profile</strong></h6><br />
        <ul class="simple-user-list">
          <li>
            <span class="title"> <i class="fa fa-phone"></i>&emsp; <?php echo $this->session->userdata('phone')?></span>
          </li>
        </ul>
        <div class="clearfix">
        </div>
      </div>
    </section>
  </div>

  <div class="col-md-8 col-lg-9 col-xl-6">
    <div class="tabs">
    </div>
  </div>
</div>
