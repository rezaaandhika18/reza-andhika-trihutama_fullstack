<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <header class="panel-heading">
        <div class="panel-actions">
          <a href="#" class="fa fa-caret-down"></a>
          <a href="#" class="fa fa-times"></a>
        </div>
        <h2 class="panel-title">Form <?=ucwords(strtolower($title))?></h2>
      </header>
      <div class="panel-body">
        <?=form_open('article/edit/submit',array('method' => 'post', 'enctype' => 'multipart/form-data')); ?>
          <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash(); ?>">
          <input type="hidden" name="id" value="<?=$articleObj->id?>"/>
          <div class="box-body">
            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Title <span class="required">*</span></label>
              <div class="form-group col-md-10 col-sm-10 col-xs-12">
                <input type="text" id="title" class="form-control" name="title" value="<?=$articleObj->title?>" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Slug <span class="required">*</span></label>
              <div class="form-group col-md-10 col-sm-10 col-xs-12">
                <input type="text" id="slug" class="form-control" name="slug" value="<?=$articleObj->slug?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Content</label>
              <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea class="form-control" id="textcontent" rows="10" cols="80" name="content"><?=$articleObj->content?></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Image</label>
              <div class="col-md-10 col-sm-10 col-xs-12">
                <input id="image" type="file" name="image" class="form-control col-md-7 col-xs-12" data-theme="fas" data-overwrite-initial="true">
                <input type="hidden" name="old_image" value=<?=$articleObj->image_name?>>
                <label>(Max 5 MB) png, jpg, jpeg</label>
              </div>
            </div>
            <hr>

            <div class="box-footer">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button type="submit" class="mb-xs mt-xs mr-xs btn btn-sm btn-success"><i class="fa fa-save"></i> Submit</button>
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-sm btn-default" onclick="window.location.href = '<?=base_url('article/list')?>'"><i class="fa fa-arrow-left"></i> Back to Article List</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
  </div>
</div>