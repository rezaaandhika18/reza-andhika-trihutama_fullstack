<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <?php if($this->input->get('success_box') == md5('true')){?>
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Well done!</strong> Article berhasil ditambahkan.
      </div>
      <?php } elseif($this->input->get('success_box') == md5('false')) { ?>
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo preg_replace("/\r\n|\r|\n/","<br>",base64_decode($this->input->get('reason')))?>
      </div>
      <?php } ?>

      <header class="panel-heading">
        <div class="panel-actions">
          <a href="#" class="fa fa-caret-down"></a>
          <a href="#" class="fa fa-times"></a>
        </div>
        <h2 class="panel-title">Request Form Article</h2>
      </header>
      <form name="form1" enctype="multipart/form-data" method="post" action="<?=base_url("article/create/submit")?>" autocomplete="off">
      <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash(); ?>">
        <div class="panel-body">
          <div class="box-body">
            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Title <span class="required">*</span></label>
              <div class="form-group col-md-10 col-sm-10 col-xs-12">
                <input type="text" id="title" class="form-control" name="title" placeholder="Write your ideas ??" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Slug <span class="required">*</span></label>
              <div class="form-group col-md-10 col-sm-10 col-xs-12">
                <input type="text" id="slug" class="form-control" name="slug" placeholder="Write your slug" required>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Content</label>
              <div class="col-md-10 col-sm-10 col-xs-12">
                <textarea class="form-control" id="textcontent" rows="10" cols="80" name="content"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 col-sm-2 col-xs-12">Image</label>
              <div class="col-md-10 col-sm-10 col-xs-12">
                <input id="image" type="file" name="image" class="form-control col-md-7 col-xs-12" data-theme="fas">
                <label>(Max 5 MB) png, jpg, jpeg</label>
              </div>
            </div>
            <hr>

            <div class="box-footer" style="text-align:center;">
              <div class="col-md-5 col-sm-5 col-xs-5 col-md-offset-3" style="text-align:center;">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
  </div>
</div>