<?php
class Usermgt_mdl extends CI_Model{
	
	function __construct() {
		parent::__construct();
    }
	
	public function validateLoginUsernamePassword($username,$password){
		$userStatus = $this->is_user_exist($username);
		if (!$userStatus){
			$resultArray = array('resultCode' => "WT_ERROR_INVALID_USERNAME_PASSWORD",'resultMsg' => "WT_ERROR_INVALID_USERNAME_NOT_EXIST");
			return $resultArray;
		}

		$wtuserObj = $this->get_valid_user($username,$password);
		if ($wtuserObj == null){
			$resultArray = array('resultCode' => "WT_ERROR_INVALID_USERNAME_PASSWORD",'resultMsg' => "WT_ERROR_INVALID_USERNAME_PASSWORD");
			return $resultArray;
		}

		$resultArray = array('resultCode' => "WT_OK",'resultMsg' => $wtuserObj);
		return $resultArray;
	}

	public function is_user_exist($username){
		$sql = "SELECT id FROM user WHERE username = '$username' AND is_active = 1";
		$resultSet = $this->db->query($sql);
		if ($resultSet->num_rows() > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function get_valid_user($username,$password){
		$sql = "SELECT * FROM user WHERE username = '$username' AND password='".md5($username.$password)."'";
		$resultSet = $this->db->query($sql);
		if ($resultSet->num_rows() > 0){
			return $resultSet->row();
		}
		else{
			return FALSE;
		}
	}

	public function saveLastLogin($username){
		$data = array('last_login' => date('Y-m-d H:i:s'));
		$resultSetUpdUser = $this->db->update("user", $data, array('username' => $username));
	}

	public function insertUser($username,$phone,$password){
        $data = array(
            'username' => $username,
            'phone'    => $phone,
			'password' => md5($username.$password),
        );

        $this->db->set('created_date', 'NOW()', FALSE);
        $result = $this->db->insert('user', $data);
        return $result;
    }

}