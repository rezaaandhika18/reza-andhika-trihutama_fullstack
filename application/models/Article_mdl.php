<?php
class Article_mdl extends CI_Model{
	
	function __construct() {
		parent::__construct();
	}

	var $table = 'article';
	
	public function insert_article($article){
        $data = array(
            'title'      => $article['title'],
            'slug'       => $article['slug'],
			'content'	 => $article['content'],
            'image_name' => $article['image_data']['orig_name'],
            'image_hash' => $article['image_data']['file_name'],
            'created_by' => $article['created_by']
        );

        $this->db->set('created_date', 'NOW()', FALSE);
        $this->db->set('last_update', 'NOW()', FALSE);
        $result = $this->db->insert($this->table, $data);
        return $result;
    }

	public function filter($search, $limit, $start, $order_field, $order_ascdesc, $username=null){
		$order_field = $order_field === "0" ? 'id' : $order_field;
		$order_ascdesc = $order_ascdesc == 'desc' ? 'desc' : $order_ascdesc;

		$this->db->select("id,title,slug,image_name,image_hash,created_by,created_date,last_update,is_active");
        $this->db->from('article');
        $this->db->like('title', $search);  
		$this->db->or_like('slug', $search);  
		$this->db->or_like('image_name', $search);
		$this->db->or_like('created_date', $search);
		$this->db->or_like('last_update', $search);
		
		$lastQuery = $this->db->get_compiled_select();
		$this->db->select('*');
		$this->db->from("($lastQuery)t1");
		if($username!=null){
			$this->db->where('created_by', $username);
		}
		$this->db->where('is_active', 1);
        $this->db->order_by($order_field, $order_ascdesc);
		$this->db->limit($limit, $start);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
        	$result = $query->result();
        	$query->free_result();
            return $result;
        } else {
            return 0;
        }
	}

	public function count_all($username=null){
		if($username!=null){
			$sql = "SELECT id FROM ".$this->table." WHERE created_by = '$username' AND is_active = 1";
		}
		else{
			$sql = "SELECT id FROM ".$this->table." WHERE is_active = 1";
		}
		
		$resultSet = $this->db->query($sql);
		$totalRow  = $resultSet->num_rows();
		$resultSet->free_result();
		return $totalRow;
	}

	public function count_filter($search, $username=null){
		$this->db->select("id,title,slug,image_name,image_hash,created_by,created_date,last_update,is_active");
        $this->db->from('article');
        $this->db->like('title', $search);  
		$this->db->or_like('slug', $search);
		$this->db->or_like('image_name', $search);
		$this->db->or_like('created_date', $search);
		$this->db->or_like('last_update', $search);
		
		$lastQuery = $this->db->get_compiled_select();
		$this->db->select('*');
		$this->db->from("($lastQuery)t1");
		if($username!=null){
			$this->db->where('created_by', $username);
		}
		$this->db->where('is_active', 1);

		$query = $this->db->get();
        if ($query->num_rows() > 0) {
        	$result = $query->num_rows();
        	$query->free_result();
            return $result;
        } else {
            return 0;
        }
	}

	public function getArticleById($id){
		$sql       = "SELECT * FROM ".$this->table." WHERE id = $id";
		$resultSet = $this->db->query($sql);
		$row 	   = $resultSet->row();
		$resultSet->free_result();
		return $row;
	}

	public function updateArticleById($article){
        $data = array(
            'title'      => $article['title'],
            'slug'       => $article['slug'],
			'content'	 => $article['content'],
            'image_name' => $article['image_data']['orig_name'],
            'image_hash' => $article['image_data']['file_name'],
        );

        $this->db->set('last_update', 'NOW()', FALSE);
        $result = $this->db->update($this->table, $data, array('id'=>$article['id']));
        return $result;
    }

	public function deleteUserById($id){
        $data = array('is_active' => 0,);
        $this->db->set('last_update', 'NOW()', FALSE);
        $result = $this->db->update($this->table, $data, array('id'=>$id));
        return $result;
    }
}
?>