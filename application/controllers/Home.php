<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct(){
        parent::__construct();
        // Your own constructor code
		$this->load->model('article_mdl');
	}

	public function landingPage(){
		$data['title'] = 'Landing Page';
		$this->load->view('lading_Page_view', $data);
	}
	// =============================================================

	public function index(){
		$data['title'] = 'Home';
		$this->template->load('template','page/home_view', $data);
	}

	public function create(){
		$data['title'] = 'Create Article';
		$this->template->load('template','page/create_article_view', $data);
	}

	public function createSubmit(){
        $username = $this->session->userdata('username');

		$this->form_validation->set_rules('title','title','required');
		$this->form_validation->set_rules('slug','slug','required');
		if (empty($_FILES['image']['name'])){
			$this->form_validation->set_rules('image', 'image', 'required');
		}
		$this->form_validation->set_message('required', 'You missed the input {field} !');

		if ($this->form_validation->run() == FALSE){
			$errors = validation_errors();
			redirect('/article/create?success_box='.md5('false').'&reason='.base64_encode($errors), 'refresh');
		}
		else{
			$article['created_by']   = $username;
			$article['title']        = $this->input->post('title');
			$article['slug']         = $this->cleanSlug($this->input->post('slug'));
			$article['content']      = $this->input->post('content');
			$article['image_result'] = $this->imageUpload();
			$article['image_data']   = $this->upload->data();

            $save = $this->article_mdl->insert_article($article);
			if($save){
				redirect('/article/create?success_box='.md5('true'), 'refresh');
			}
			else{
				$errors = "Failed insert article";
				redirect('/article/create?success_box='.md5('false').'&reason='.base64_encode($errors), 'refresh');
			}
		}
	}

	public function listArticle(){
		$data['title'] = 'List Article';
		$this->template->load('template','page/list_article_view', $data);
	}

	public function getListArticle(){
		$username 	  = $this->session->userdata('username');
		$search        = $this->input->post('search')['value'];
		$limit         = $this->input->post('length');
		$start         = $this->input->post('start');
		$order_index   = $this->input->post('order')[0]['column'];
		$order_field   = $this->input->post('columns')[$order_index]['data'];
		$order_ascdesc = $this->input->post('order')[0]['dir'];
		$sql_total     = $this->article_mdl->count_all($username);
		$sql_data      = $this->article_mdl->filter($search, $limit, $start, $order_field, $order_ascdesc, $username );
		$sql_filter    = $this->article_mdl->count_filter($search, $username );

		$callback = array(
						'recordsTotal'    => $sql_total,
						'recordsFiltered' => $sql_filter,
						'data'            => $sql_data
					);

		header('Content-Type: application/json');
		echo json_encode($callback); 
		return;
	}

	public function editArticle(){
		$uri      = $this->uri->segment(3);
		$username = $this->session->userdata('username');
		$subData  = null;

		if($uri==="submit"){
			$this->form_validation->set_rules('title','title','required');
			$this->form_validation->set_rules('slug','slug','required');
			$this->form_validation->set_message('required', 'You missed the input {field} !');

			if ($this->form_validation->run() == FALSE){
				$errors = validation_errors();
				redirect('/article/list?success_box='.md5('false').'&reason='.base64_encode($errors), 'refresh');
			}
			else{
				$id				    = $this->input->post('id');
				$articleObj         = $this->article_mdl->getArticleById($id);
				$article['id']      = $this->input->post('id');
				$article['title']   = $this->input->post('title');
				$article['slug']    = $this->input->post('slug');
				$article['content'] = $this->input->post('content');

				if(isset($_FILES['image'])){
					if($articleObj->image_name == $this->input->post('old_image') && empty($_FILES['image']['name'])){
						$article['image_data'] = array('orig_name' => $articleObj->image_name,'file_name' => $articleObj->image_hash);
					}
					else{
						$article['image_result'] = $this->imageUpload();
						$article['image_data']   = $this->upload->data();
					}
				}
				else{
					$errors = 'Image mandatory / required';
					redirect('/article/list?success_box='.md5('false').'&reason='.base64_encode($errors), 'refresh');
				}

				$update = $this->article_mdl->updateArticleById($article);
				if($update){
					$reason = "Update article success - ".$article['title'];
					redirect('/article/list?success_box='.md5('true').'&reason='.base64_encode($reason), 'refresh');
				}
				else{
					$errors = "Failed insert article - ".$article['title'];;
					redirect('/article/list?success_box='.md5('false').'&reason='.base64_encode($errors), 'refresh');
				}
			}
		}

		$articleObj 		= $this->article_mdl->getArticleById(base64_decode(base64_decode($this->input->get('id'))));
		$data['articleObj'] = (object) $articleObj;
		$data['title']      = 'Edit Article';

		$this->template->load("template","page/edit_article_view",$data);
	}

	public function deleteArticle(){
		$username     = $this->session->userdata('username');
		$id           = base64_decode(base64_decode($this->input->get('id')));
		$articleObj   = $this->article_mdl->getArticleById($id);
		$resultDelete = $this->article_mdl->deleteUserById(base64_decode(base64_decode($this->input->get('id'))));

		if($resultDelete){
			$reason = "Delete article success - ".$articleObj->title;
			redirect('article/list?success_box='.base64_encode('true').'&reason='.base64_encode($reason));
		}else{
			$errors = "Failed delete article - ".$articleObj->title;
			redirect('/article/list?success_box='.md5('false').'&reason='.base64_encode($errors), 'refresh');
		}
	}

	// =======================================================
	public function imageUpload(){
        $config['upload_path']   = $this->config->item('PATH_UPLOAD_IMAGE');
        $config['allowed_types'] = 'png|jpg|jpeg';
        $config['max_size']      = '5048';
        $config['encrypt_name']  = true;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        $result = $this->upload->do_upload('image');
        return $result;
    }

	public function cleanSlug($string) {
		$string = str_replace(' ', '-', $string);
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string);
	}

	public function updateCSRF(){
		$data[$this->security->get_csrf_token_name()] = $this->security->get_csrf_hash();
		echo json_encode($data); 
	}
}
