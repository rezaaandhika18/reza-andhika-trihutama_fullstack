<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {
	public function __construct(){
        parent::__construct();
        // Your own constructor code
		$this->load->model('article_mdl');
	}

	public function index(){
		$data['title'] = 'Landing Page';
		$this->load->view('landing_page_view', $data);
	}

	public function article(){
		$data['title'] = 'Landing Article';
		$this->load->view('landing_article_view', $data);
	}
}
