<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends MY_Controller {
	public function __construct(){
        parent::__construct();
        // Your own constructor code
	}
	 
	public function my404(){
		$data['title'] = '404';
		$this->template->load('template','template/err404', $data);
	}
}
