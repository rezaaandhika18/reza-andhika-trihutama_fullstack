<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'third_party/REST_Controller.php';
require APPPATH . 'third_party/Format.php';
require APPPATH . '../vendor/autoload.php';
use Restserver\Libraries\REST_Controller;
use \Firebase\JWT\JWT;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");


class Rest extends REST_Controller{

	public function __construct(){
	    parent::__construct();

	    // Load model
	    $this->load->model('article_mdl');
		$this->load->model('usermgt_mdl');
	}

	private $secretkey = 'ABCDEF123ERD456EABCDEF123ERD456E';

	private function checkJwt(){
		$jwt = $this->input->get_request_header('Authorization');
		try {
			if($jwt == null){
				return $this->response([
					'status'   => '9999',
					'message'  => 'Not authorized'
				  ], REST_Controller::HTTP_BAD_REQUEST);
			}
			else{
				$jwt = explode(' ',$jwt)[1];
				$decode = JWT::decode($jwt, $this->secretkey, array('HS256'));
				return true;
			}
		} catch (Exception $e) {
			return $this->response([
				'status'   => '9999',
				'message'  => 'Not authorized'
			  ], REST_Controller::HTTP_BAD_REQUEST);
		}
	}

	public function login_post(){
        $u   = $this->post('username');
        $p   = $this->post('password');

		$loginUser = $this->usermgt_mdl->validateLoginUsernamePassword($u, $p);
		if ($loginUser['resultCode'] != "WT_OK"){
			return $this->response([
				'status'   => '9999',
				'message'  => 'Login failed '.$loginUser['resultMsg']
			  ], REST_Controller::HTTP_BAD_REQUEST);
			$this->session->sess_destroy();
		}

		// update last login
		$saveLog = $this->usermgt_mdl->saveLastLogin($u);
		$customdata = array(
			'username' => $loginUser['resultMsg']->username,
			'phone'    => $loginUser['resultMsg']->phone,
			'is_active'=> $loginUser['resultMsg']->is_active,
		);

		$payload['iss']      = 'cms-article';
		$payload['username'] = $u;
		$payload['iat']      = time();
		$payload['exp']      = time() + 3600;
		$payload['token']     = JWT::encode($payload, $this->secretkey);

		return $this->response($payload, REST_Controller::HTTP_OK);
	}

	public function listArticle_post(){
		$this->checkJwt();

		$search        = $this->post('search');
		$limit         = $this->post('limit');
		$start         = $this->post('start');
		$order_field   = 'id';
		$order_ascdesc = 'desc';
		$sql_total     = $this->article_mdl->count_all();
		$sql_data      = $this->article_mdl->filter($search, $limit, $start, $order_field, $order_ascdesc);
		$sql_filter    = $this->article_mdl->count_filter($search);

		$callback = array(
						'recordsTotal'    => $sql_total,
						'recordsFiltered' => $sql_filter,
						'data'            => $sql_data
					);

		$result['status'] = '0000';
		$result['data']   = $callback;
		return $this->response($result, REST_Controller::HTTP_OK);
	}

	public function detailArticle_post(){
		$this->checkJwt();

		$id         = $this->post('id');
		$articleObj = $this->article_mdl->getArticleById($id);

		$result['status'] = '0000';
		$result['data']   = $articleObj;
		return $this->response($result, REST_Controller::HTTP_OK);
	}
}
