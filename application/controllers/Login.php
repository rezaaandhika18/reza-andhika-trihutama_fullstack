<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
        parent::__construct();
		$this->load->helper('url');
		$this->load->helper('cookie');
		$this->load->model('usermgt_mdl');
	}

	public function sign_in(){
		delete_cookie('csrf_cookie_name');
		$this->security->csrf_set_cookie();
		
		$data['title']      = "Login Form";
		$user_access_status = $this->uri->segment(2);
		if ( $user_access_status != "" || $user_access_status != null){
			$data['user_alert']        = "<script> alert(\"Username and Password not found in system CMS\"); </script>";
			$data['user_validate_msg'] = "Input valid Username dan Password ";
		}
		else{
			$data['user_alert'] ="";
			$data['user_validate_msg'] = "Please input username and password";
		}

		$this->load->view('sign_in_view', $data);
	}

	public function serviceSign_in(){
		$postInput = $this->input->post();
		$loginUser = $this->usermgt_mdl->validateLoginUsernamePassword($postInput['username'], $postInput['password']);
		if ($loginUser['resultCode'] != "WT_OK"){
			$this->session->sess_destroy();

			//return to view
			$reason = 'Login failed, '.$loginUser['resultMsg'];
			redirect('/sign_in?notifbox='.base64_encode('false').'&reason='.base64_encode($reason), 'refresh');
		}

		// update last login
		$saveLog = $this->usermgt_mdl->saveLastLogin($postInput['username']);
		$customdata = array(
			'username' => $loginUser['resultMsg']->username,
			'phone'    => $loginUser['resultMsg']->phone,
			'is_active'=> $loginUser['resultMsg']->is_active,
		);
		
		$this->session->set_userdata($customdata);
		echo "<script type='text/javascript'>window.location.href='".base_url('home')."'</script>";
	}

	public function sign_up(){
		// delete and replace new one
		delete_cookie('csrf_cookie_name');
		$this->security->csrf_set_cookie();
		$data['title'] = "Sign Up";
		$this->load->view('sign_up_view', $data);
	}

	public function serviceSign_up(){
		$username = $this->input->post('username');

		if(strlen($username) < 4){
			echo "<script> alert(\"Username min 4 character\"); </script>";
			redirect('/sign_up', 'refresh');
		}

		$phone         = $this->input->post('phone');
		$password      = $this->input->post('password');
		$rytpepassword = $this->input->post('rytpepassword');

		if($password != $rytpepassword){
			echo "<script> alert(\"Password and retype-password do not match\"); </script>";
			redirect('/sign_up', 'refresh');
		}

		$checkPass = $this->checkPasswordValidity($password);
		if($checkPass->resultCode == 'success'){
			$save = $this->usermgt_mdl->insertUser($username,$phone,$password);
			echo "<script> alert(\"success sign up, please sign in\"); </script>";
			redirect('/sign_in', 'refresh');
		}
		else{
			$message = json_encode($checkPass->resultMsg);
			echo "<script> alert('$message'); </script>";
			redirect('/sign_up', 'refresh');
		}
	}

	public function sign_out(){
		$this->session->sess_destroy();
		redirect("/login", 'refresh');
	}

	// ============================================================
	function checkPasswordValidity($password){
		$r1='/[A-Z]/';
		$r2='/[a-z]/'; 
		$r3='/[_&#$!@]/'; 
		$r4='/[0-9]/';
		$checkOk       = TRUE;
		$resultMessage = TRUE;
		
		if(preg_match_all($r1,$password, $o)<1){
			$checkOk = FALSE;
			$resultMessage = $resultMessage."\n-Password minimal have 1 uppercase";
		}
		
		if(preg_match_all($r2,$password, $o)<1){
			$checkOk = FALSE;
			$resultMessage = $resultMessage."\n-Password minimal have 1 lowcase";
		}

		if(preg_match_all($r3,$password, $o)<1){
			$checkOk = FALSE;
			$resultMessage = $resultMessage."\n-Password minimal have 1 special character _, #, &, !,$ atau @";
		}

		if(preg_match_all($r4,$password, $o)<1){
			$checkOk = FALSE;
			$resultMessage = $resultMessage."\n-Password minimal have 1 numberic";
		}

		if(strlen($password)<8){
			$checkOk = FALSE;
			$resultMessage = $resultMessage."\n-Password minimal have 8 character";
		}
		
		if($checkOk){
			$resultArray = (object) array( 'resultCode' => 'success','resultMsg' => 'success checking');
			return $resultArray;
		}
		else{
			$resultArray = (object) array( 'resultCode' => 'failed','resultMsg' => $resultMessage);
			return $resultArray;
		}
	}
}
