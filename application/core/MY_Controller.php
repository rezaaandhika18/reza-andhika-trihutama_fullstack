<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	private $isCsrfTokenValid = FALSE;
	
    public function __construct()
    {
        parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('encrypt');
		$this->is_user_logged_in(); // check session
		$this->is_csrf_token_valid(); // check cookie
    }

	public function get_isCsrfTokenValid(){
		return $this->isCsrfTokenValid;
	}

	public function is_user_logged_in(){
		$is_logged_in = $this->session->userdata('is_active');
		if(!isset($is_logged_in) || ($is_logged_in != true)){
			$stringUri = $this->uri->uri_string();
			if($stringUri!=""){
				echo '<script>alert("SESSION ANDA BELUM TERDAFTAR ATAU SUDAH EXPIRED , SILAHKAN LOGIN TERLEBIH DAHULU");</script>';
				$logText = "SESSION ANDA BELUM TERDAFTAR DI DATABASE";
				//Kick Access to Login Page
				$this->session->sess_destroy();
				redirect('/login', 'refresh');
			}
		}
	}

	public function is_csrf_token_valid(){
		$csrfTokenName = $this->security->get_csrf_token_name();
		if(isset($_COOKIE[$csrfTokenName])) {
			if ($input_csrf_token_value ==""){
				echo 'CSRFTOKENERROR - TOKEN CSRF TIDAK DITEMUKAN';
				echo '<script>alert("TOKEN CSRF TIDAK DITEMUKAN");</script>';

				//Kick Access to Login Page
				$this->isCsrfTokenValid = FALSE;
				die();
			}
			else{
				echo 'CSRFTOKENERROR - TOKEN CSRF TIDAK DITEMUKAN';
				echo '<script>alert("TOKEN CSRF TIDAK DITEMUKAN");</script>';

				//Kick Access to Login Page
				$this->isCsrfTokenValid = FALSE;
				die();
			}
		}
	}
}
?>
