<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
	|--------------------------------------------------------------------------
	| WTUSERMGT DB CONSTANT Stream Modes
	|--------------------------------------------------------------------------
	|
	| These modes are used when working with fopen()/popen()
	|
 */
define('WT_OK',										1);

define('WT_OK_TRANSACTION_COMPLETED',				1);
define('WT_ERROR',									0);

define('WT_ERROR_INVALID_USERNAME_PASSWORD',		0);
define('WT_ERROR_USER_NOT_FOUND',					0);

define('WT_OK_EMAIL_VALID',							7001);
define('WT_ERROR_INVALID_EMAIL_ADDRESS',			7009);

define('WT_OK_USER_EXIST_AND_PASSWORD_ACTIVE',		8001);
define('WT_OK_USER_EXIST_IN_DB',					8002);
define('WT_OK_TRANSACTION_ROLLBACK_COMPLETED',		8009);

define('WT_OK_USER_LOGOUT_PROCESS',					8100);
define('WT_OK_USER_LOGIN_PROCESS',					8101);
define('WT_OK_USER_UPDATE_PASSWORD',				8102);
define('WT_OK_USER_SIGNOUT_PROCESS',				8103);

define('WT_ERROR_USERNAME_EXPIRED',					8901);
define('WT_ERROR_USERNAME_BLOCKED',					8902);
define('WT_ERROR_USERNAME_ALREADY_EXIST',			8903);
define('WT_ERROR_LOGIN_ATTEMPT_EXCEEDED',			8904);
define('WT_ERROR_WAIT_BEFORE_NEXT_LOGIN',			8905);

define('WT_WARNING_PASSWORD_EXPIRED',				9000);
define('WT_OK_PASSWORD_PATTERN_VALID',				9001);
define('WT_ERROR_INVALID_PASSWORD_PATTERN2',		9002);
define('WT_ERROR_INVALID_PASSWORD_PATTERN3',		9003);
define('WT_ERROR_INVALID_PASSWORD_PATTERN4',		9004);
define('WT_ERROR_INVALID_PASSWORD_PATTERN5',		9005);
define('WT_ERROR_PASSWORD_HIST_FOUND',				9006);
define('WT_ERROR_PASSWORD_HIST_RENEWED',			9007);

define('WT_ERROR_PASSWORD_HIST_NOT_FOUND',			9007);
define('WT_ERROR_PASSWORD_EXPIRED',					9008);
define('WT_ERROR_WRONG_PASSWORD',					9009);
define('WT_ERROR_WRONG_USERNAME',					9010);
define('WT_ERROR_USERNAME_NOT_FOUND',				9010);
define('WT_ERROR_PASSWORD',							9011);
define('WT_ERROR_PASSWORD_BEEN_USED_BEFORE',		9012);

define('WT_ERROR_INSERT_DB_USERS',					9100);
define('WT_ERROR_UPDATE_DB_USERS',					9101);
define('WT_ERROR_DELETE_DB_USERS',					9102);
define('WT_ERROR_TRANSACTION_DB_USERS',				9109);
define('WT_ERROR_INSERT_DB_USER_PWD_HIST',			9200);
define('WT_ERROR_UPDATE_DB_USER_PWD_HIST',			9201);
define('WT_ERROR_DELETE_DB_USER_PWD_HIST',			9202);
define('WT_ERROR_TRANSACTION_DB_USER_PWD_HIST',		9209);
define('WT_ERROR_INSERT_DB_SESSIONS',				9300);
define('WT_ERROR_UPDATE_DB_SESSIONS',				9301);
define('WT_ERROR_DELETE_DB_SESSIONS',				9302);
define('WT_ERROR_TRANSACTION_DB_SESSIONS',			9309);



/* End of file constants.php */
/* Location: ./application/config/constants.php */