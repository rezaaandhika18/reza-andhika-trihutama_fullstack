/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	// alert(CKEDITOR.version);
	//config.filebrowserBrowseUrl      = 	'../../kcfinder/browse.php?opener=ckeditor&type=files';
	//config.filebrowserImageBrowseUrl =  '../../kcfinder/browse.php?opener=ckeditor&type=images';
	//config.filebrowserFlashBrowseUrl =  '../../kcfinder/browse.php?opener=ckeditor&type=flash';
	//config.filebrowserUploadUrl      =  '../../kcfinder/upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl =  './../kcfinder/upload.php?opener=ckeditor&type=images';
	//config.filebrowserFlashUploadUrl =  '../../kcfinder/upload.php?opener=ckeditor&type=flash';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links','EasyImageUpload' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	/*config.removeButtons = 'Underline,Subscript,Superscript';*/
	config.removeButtons = 'Save,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,RemoveFormat,CopyFormatting,Language,Flash,PageBreak';
	config.removeDialogTabs = 'image:Link;image:advanced';
	// Set the most common block elements.
	//config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	//\config.removeDialogTabs = 'image:advanced;link:advanced';
};