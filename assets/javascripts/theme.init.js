//base url bila sudah di upload di sesuaikan lagi base url nya
var getUrl   = window.location;
var base_url = getUrl .protocol + "//" + getUrl.host + "/";

function get_ajax_content(link,targetElement,boolasynch){
	// start loading
	loaderSpin(targetElement);
	$.ajax({
		url : base_url + link ,
		type : 'GET',
		async: boolasynch,
		success: function(result){
			if (result){
				if (result.indexOf("LOGINERROR")>=0){
					alert(result);
					window.location = base_url;
				}
				else{					
					$(targetElement).html(result);
				}
			}
		}
	});
	$( document ).ajaxStop(function() {
		$( "#loader" ).css("visibility","hidden");
	});
};

function loaderSpin(idDiv = null){
	var spin = '<div class="col-md-3 col-sm-3 col-xs-12" id="loader">'+
					'<i class="fa fa-spinner fa-spin"></i> Loading'+
				'</div>';

	$( ""+idDiv+"" ).html( spin );
}

function post_ajax_content(formElement, link,targetElement,boolasynch){
	var service_url = link;
	var form_param  = $(formElement).serialize();

	loaderSpin(targetElement);
	$.ajax({
		type : "POST",
		url : service_url,
		data : form_param,
		async: true,
		success: function(res){
			var html = '<section class="panel">'+
						  '<header class="panel-heading">'+
						    '<div class="panel-actions">'+
						    '</div>'+
						  '</header>'+
						  '<div class="panel-body">'+
						  res+
						  '</div>'+
						'</section>';
			$(targetElement).html(html);
		}
	});
	$( document ).ajaxStop(function() {
		$( "#loader" ).css("visibility","hidden");
	});
};

// Datepicker
(function( $ ) {

	'use strict';

	if ( $.isFunction($.fn[ 'datepicker' ]) ) {

		$(function() {
			$('[data-plugin-datepicker]').each(function() {
				var $this = $( this ),
					opts = {};

				var pluginOptions = $this.data('plugin-options');
				if (pluginOptions)
					opts = pluginOptions;

				$this.themePluginDatePicker(opts);
			});
		});

	}

}).apply(this, [ jQuery ]);

// Sidebar
function init_sidebar() {
    var CURRENT_URL = window.location.href.split('#')[0].split('?')[0], $SIDEBAR_MENU = $('#menu');

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('nav-active').parent('ul').parent('li').addClass('nav-active nav-expanded');

    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href == CURRENT_URL;
    }).parent('li').addClass('').parents('ul').slideDown();
}

// call function
init_sidebar();