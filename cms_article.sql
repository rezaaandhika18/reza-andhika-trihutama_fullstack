-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: cms_article
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `slug` text NOT NULL,
  `content` text,
  `image_hash` varchar(200) DEFAULT NULL,
  `image_name` varchar(200) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `is_active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'Dinkes Jakarta: Seluruh Puskesmas Siap Layani Vaksinasi Booster bagi Lansia','Dinkes-Jakarta-Seluruh-Puskesmas-Siap-Layani-Vaksinasi-Booster-bagi-Lansia','<p>Penyuntikan vaksin Covid-19&nbsp;ketiga&nbsp;dimulai hari ini, Rabu (12/1/2022). Dinas Kesehatan (Dinkes) DKI Jakarta memastikan kesiapan seluruh puskesmas di Jakarta untuk melayani <a href=\"https://www.liputan6.com/tag/vaksinasi-booster\">vaksinasi&nbsp;<em>booster</em></a><em>&nbsp;</em>tersebut.</p>\r\n\r\n<p>&quot;Kita sudah infokan ke semua puskesmas, nanti kita lihat seberapa banyak sasaran bisa sempat hadir,&quot; kata Kepala Dinas Kesehatan DKI Jakarta Widyastuti pada wartawan, Selasa malam, 11 Januari 2022.</p>\r\n\r\n<p>Widyastuti memastikan, akan dibuat alur antrean yang rapi dan tidak menimbulkan kerumunan pemberian <a href=\"https://www.liputan6.com/tag/vaksin-booster\">vaksin <em>booster</em></a>, sehingga lansia dapat terlayani dengan baik. &quot;Karena dibutuhkan alur yang lebih rapi karena lansia kasihan ya, kalau enggak diatur dengan rapi,&quot; ujar dia.</p>\r\n\r\n<p>Dia memastikan, seluruh puskesmas siap menerima vaksinasi <em>booster</em> bagi para lansia di Jakarta. &quot;Intinya teman-teman puskesmas sudah siap menerima lansia yang akan melakukan vaksinasi (<em>booster</em>),&quot; kata dia.</p>\r\n\r\n<p>Sementara itu, Wakil Gubernur DKI Jakarta Ahmad Riza Patria menyebut untuk saat ini, vaksinasi <em>booster</em> diprioritaskan kepada golongan lansia dan kelompok rentan. Adapun varian vaksin <a href=\"https://www.liputan6.com/tag/covid-19\">Covid-19</a> yang disediakan adalah Sinovac, AstraZeneca, Pfizer, dan Moderna.</p>\r\n\r\n<p>&quot;Ada Sinovac, Pfizer, AstraZeneca, Moderna. Angkanya belum ada,&quot; kata dia.</p>\r\n\r\n<p>&nbsp;</p>\r\n','d44fc194859c6b1458bed427829b3750.jpg','VAKSIN-DOSIS-KETIGA.jpg','2022-01-12 12:31:07','maximal','2022-01-12 12:31:07',1),(2,'5 Lokasi SIM Keliling di Jakarta Hari Ini, Rabu 12 Januari 2022','5-Lokasi-SIM-Keliling-di-Jakarta-Hari-Ini-Rabu-12-Januari-2022','<p>Pemilik SIM atau Surat Izin Pengemudi wajib mengetahui batas masa berlaku&nbsp;<a href=\"https://www.liputan6.com/tag/sim\">SIM</a>&nbsp;miliknya. Jika masa berlaku sampai terlewat, maka pemilik SIM harus mengajukan SIM yang baru.</p>\r\n\r\n<p>Bagi Anda yang sedang berada di Jakarta dan ingin melakukan perpanjangan SIM hari ini,&nbsp;Rabu (12 Januari 2022), Direktorat Lalu Lintas Polda Metro Jaya menyediakan layanan&nbsp;<a href=\"https://www.liputan6.com/tag/sim-keliling\">SIM Keliling</a>&nbsp;di 5 lokasi.</p>\r\n\r\n<p>Anda wajib mematuhi protokol kesehatan saat mengunjungi lokasi SIM Keliling. Selalu pakai masker dan lakukan&nbsp;<em>physical distancing</em>.</p>\r\n\r\n<p>Berdasarkan unggahan @TMCPoldaMetro di Twitter pagi ini,&nbsp;Rabu (12 Januari 2022), SIM Keliling beroperasi dari jam 08.00 hingga 14.00 WIB.</p>\r\n\r\n<p>Dilansir&nbsp;<em>NTMCPolri</em>, layanan SIM Keliling Polda Metro Jaya hanya melayani permohonan perpanjangan&nbsp;<a href=\"https://www.liputan6.com/tag/sim-a\">SIM A</a>&nbsp;dan C yang dapat dilakukan sebelum masa berlaku habis.</p>\r\n\r\n<p>&nbsp;</p>\r\n','e539a16ed4822265d6e1b4ce85628c8a.jpg','VAKSIN-DOSIS-KETIGA.jpg','2022-01-12 12:37:55','maximal','2022-01-12 15:36:37',0),(3,'Disdik DKI Akan Ubah Metode PTM Jika PPKM Jakarta Naik Level 3','Disdik-DKI-Akan-Ubah-Metode-PTM-Jika-PPKM-Jakarta-Naik-Level-3','<p>Dinas Pendidikan DKI Jakarta mengubah skema Pembelajaran Tatap Muka (PTM) dengan kapasitas 100 persen jika status <a href=\"https://www.liputan6.com/tag/ppkm-level-3\">PPKM</a> di Ibu Kota naik menjadi level tiga.</p>\r\n\r\n<p>&quot;Jika wilayah DKI memasuki PPKM Level 3 maka skenario PTM diubah menjadi seperti pada tahun 2021,&quot; kata Kepala Bagian Humas Dinas Pendidikan DKI Jakarta, Taga Radja Gah di Jakarta, Selasa (11/1/2022).</p>\r\n\r\n<p>Pada pelaksanaan PTM terbatas pada 2021, jadwal pembelajaran hanya dilaksanakan tiga hari, Senin, Rabu dan Jumat. Sedangkan kapasitas kelas dibuka hanya 50 persen serta pembelajaran hanya empat jam.</p>\r\n\r\n<p>Untuk metode pembelajaran dilakukan campuran, yakni sebagian di rumah dan sebagian tatap muka.</p>\r\n\r\n<p>&quot;Tapi kalau PPKM Level 4, sesuai petunjuk teknis dan surat keputusan bersama empat menteri, semua pembelajarannya daring,&quot; katanya yang dikutip dari <em>Antara</em>.</p>\r\n','76fa227dbd826368b3b36391b4bf0927.jpg','Gelombang-Ketiga-COVID-19-5.jpg','2022-01-12 15:55:51','maximal','2022-01-12 15:56:19',1);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `is_active` int(2) DEFAULT '1',
  `created_date` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'maximal','532794cb4a9117d3211bc697c86faa16','6287782336565',1,'2022-01-12 09:00:00','2022-01-12 11:37:17'),(2,'sasa','6775099cbca9fca29ec1ce86a6632e81','6282125363200',1,'2022-01-12 17:17:20',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-12 22:23:21
