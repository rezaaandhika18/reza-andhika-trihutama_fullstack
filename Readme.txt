WEB CMS ARTILCE :
1. Clone Cms Article
   a. run composer install
   b. wait until vendor done installed
2. Database --> cms_article.sql
   a. create database
   b. import to database (poin 2.a)
3. Set configuration --> cms_article/application/config
4. Sign in test :
    username = maximal
    password = Rabu123!

API ARTICLE :
Login ---
url : 127.0.0.1/cms_article/api/login
parameter : username, password

List All Article ---
url : 127.0.0.1/cms_article/api/list/article
header : authorization, type bearer token (use token jwt from login)
parameter : -

List Detail Article ---
url : 127.0.0.1/cms_article/api/detail/article
header : authorization, type bearer token (use token jwt from login)
parameter : id