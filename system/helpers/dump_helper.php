<?php
/**
 * CodeIgniter Dump Helpers
 * 
 * @package CodeIgniter
 * @category Helpers
 * @author Kroxfrom
 * @version 1.1
 */
 
if ( ! function_exists('dd')){
    function dd(){
        list($callee) = debug_backtrace();
        $args = func_get_args();
        $total_args = func_num_args();
        echo '<div><fieldset style="background: #fefefe !important; border:1px red solid; padding:15px">';
        echo '<legend style="background:blue; color:white; padding:5px;">'.$callee['file'].' @line: '.$callee['line'].'</legend><pre><code>';
        $i = 0;
        foreach ($args as $arg){
            echo '<strong>Debug #' . ++$i . ' of ' . $total_args . '</strong>: ' . '<br>';

            var_dump($arg);
        }
        echo "</code></pre></fieldset><div><br>";
        die();
    }
} 

/* End of file dump_helper.php */
/* Location: ./application/helpers/dump_helper.php */